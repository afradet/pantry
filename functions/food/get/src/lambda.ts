import { Food } from "@pantry/model";
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Context,
} from "aws-lambda";

export const handler = async (
  event: APIGatewayProxyEvent,
  context: Context
): Promise<APIGatewayProxyResult> => {
  const food: Food = {
    id: 1,
    name: "My food from lambda",
  };

  return {
    statusCode: 200,
    body: JSON.stringify({
      food,
    }),
  };
};
