# Pantry management app

## CLI

### Install
```sh
yarn install 
```
### Build
Build libs and generate dedicated lock file for lambdas
```sh
yarn workspaces run build
```
### Deploy
```sh
cdk synth
cdk deploy
```

## Thanks
Monorepo moslty based on **Martijn-Sturm** cdk-monorepo-ts :
- [Github](https://github.com/Martijn-Sturm/cdk-monorepo-ts/tree/main)
- [Article](https://martijn-sturm.hashnode.dev/typescript-monorepo-aws-infra-lambdas-cdk#how-do-workspaces-resolve-dependencies)