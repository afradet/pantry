import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import { LambdaConstruct } from "./lambda-construct";

export class PantryInfraStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const lambdaName= 'food-get';
    new LambdaConstruct(this, lambdaName, {
      lambdaName,
      lambdaRootDir: "../functions/food/get",
      handlerFilePath: "src/lambda.ts",
    });
  }
}
