import { Runtime } from "aws-cdk-lib/aws-lambda";
import { Construct } from "constructs";
import * as lambda from "aws-cdk-lib/aws-lambda-nodejs";

export interface LambdaProps {
  lambdaName: string,
  lambdaRootDir: string;
  handlerFilePath: string;
}

export class LambdaConstruct extends Construct {
  public readonly props: LambdaProps;
  public readonly lambda: lambda.NodejsFunction;

  constructor(scope: Construct, id: string, props: LambdaProps) {
    super(scope, id);
    this.props = props;

    this.lambda = new lambda.NodejsFunction(this, props.lambdaName, {
      entry: this.getHandlerModulePath(
        props.lambdaRootDir,
        props.handlerFilePath
      ),
      depsLockFilePath: `${props.lambdaRootDir}/yarn.lock`,
      runtime: Runtime.NODEJS_18_X,

    });
  }

  private getHandlerModulePath(rootFolderPath: string, moduleFileName: string) {
    return `${rootFolderPath}/${moduleFileName}`;
  }
}
