#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { PantryInfraStack } from '../lib/pantry-infra-stack';

const app = new cdk.App();
new PantryInfraStack(app, 'PantryInfraStack', {
  env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: 'eu-west-1' }
});
